<?php
include '../koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 0038XXXXXXX',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. KIOS MALASNGODING',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.malasngoding.com email : malasngoding@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Jenis",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(8, 0.8, 'Nama Jenis', 1, 0, 'C');
$pdf->Cell(8, 0.8, 'Kode Jenis', 1, 0, 'C');
$pdf->Cell(8, 0.8, 'keterangan', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysql_query ("SELECT * FROM jenis");
       while ($lihat = mysql_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(8, 0.8, $lihat['nama_jenis'],1, 0, 'C');
	$pdf->Cell(8, 0.8, $lihat['kode_jenis'], 1, 0,'C');
	$pdf->Cell(8., 0.8, $lihat['keterangan_jenis'],1, 1, 'C');

	$no++;
}

$pdf->Output("laporan_jenis.pdf","I");

?>


