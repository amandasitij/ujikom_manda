n<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
  </style>

<?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data pengembalian pegawai.xls");
?>

<center>
  <h1>Data pengembalian pegawai</h1>
</center>

<table border="1">
 <thead>
    <tr>
    	<th>#</th>
		 <th>Nama Peminjam</th>
            <th>Kode Peminjam</th>
            <th>Tanggal Pinjam</th>
            <th>Status Peminjaman</th>
    </tr>
    </thead>
    <tbody>
	<?php
	    include "../koneksi.php";
	    $no=1;
	    $select = mysql_query("SELECT * FROM peminjaman2 pe LEFT JOIN detail_pinjam2 d ON pe.id_peminjaman2=d.id_peminjaman2 LEFT JOIN pegawai pg ON pe.id_pegawai=pg.id_pegawai LEFT JOIN inventaris i ON i.id_inventaris = d.id_inventaris");
          while($data=mysql_fetch_array($sql)){
    ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td class='text-center'><?=$data['nama_pegawai']?></td>
          <td class='text-center'><?=$data['kode_peminjaman2']?></td>
          <td class='text-center'><?=$data['tanggal_pinjam']?></td>
          <td class='text-center'><?=$data['status_peminjaman2']?></td>
    </tr>
	<?php
		}
	?>
    </tbody>
</table>
                                 
</body>
</html>