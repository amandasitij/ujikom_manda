<?php 
 include('head.php');
 ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content">
      <h3>KATEGORI PERJURUSAN</h3>
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-6 col-xs-8">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>RPL</h3><br>
              </div>
              <div class="icon">
                <img src="../images/rpl.jpg" width="70px" height="70px">              
                </div>
              <a href="rpllllcadang.php?jurusan=RPL" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>BC</h3><br>
              </div>
              <div class="icon">
                <img src="../images/bc.jpg" width="70px" height="70px"> 
              </div>
              <a href="rpllllcadang.php?jurusan=BC" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>TKR</h3><br>
              </div>
              <div class="icon">
                <img src="../images/tkr.jpeg" width="70px" height="70px"> 
              </div>
              <a href="rpllllcadang.php?jurusan=TKR" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>TPL</h3><br>
              </div>
              <div class="icon">
                <img src="../images/tpl.jpg" width="70px" height="70px"> 
              </div>
              <a href="rpllllcadang.php?jurusan=TPL" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>ANM</h3><br>
              </div>
              <div class="icon">
                <img src="../images/anm.jpg" width="70px" height="70px"> 
              </div>
              <a href="rpllllcadang.php?jurusan=animasi" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>LAINNYA</h3><br>
              </div>
              <div class="icon">
                <img src="../images/lainnya.png" width="70px" height="70px"> 
              </div>
              <a href="rpllllcadang.php?jurusan=lainnya" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
              
         
  <div class="modal modal-primary fade" id="modal-info">
        <div class="modal-dialog" style="width:470px;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah data barang</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" action="tambah_inventaris.php" method="POST">
                  <input type="hidden" name="id_inventaris" value="<?php echo $data['id'] ?>">
                  <div class="row">
                  <div class="form-group">
                    <label for="nama" class="col-sm-2 control-label label-input-sm">Nama</label>
                    <div class="col-sm-10">
                      <input type="text"  name="nama" class="form-control input-sm" required="" id="nama" placeholder="nama" value="<?php echo $data['nama'];?>">
                    </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="form-group">
                    <label for="kondisi" class="col-sm-2 control-label label-input-sm">Kondisi</label>
                    <div class="col-sm-10">
                      <input type="text" name="kondisi" class="form-control input-sm" required="" id="kondisi" placeholder="kondisi" value="<?php echo $data['kondisi'];?>">
                    </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="form-group">
                    <label for="keterangan" class="col-sm-2 control-label label-input-sm">Keterangan</label>
                    <div class="col-sm-10">
                      <input type="text" name="keterangan" class="form-control input-sm" required="" id="keterangan" placeholder="keterangan" value="<?php echo $data['keterangan'];?>">
                    </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="form-group">
                    <label for="jumlah" class="col-sm-2 control-label label-input-sm">Jumlah</label>
                    <div class="col-sm-10">
                      <input type="text" name="jumlah" class="form-control input-sm" required="" id="jumlah" placeholder="jumlah" value="<?php echo $data['jumlah'];?>">
                    </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="form-group">
                    <label for="jenis" class="col-sm-2 control-label label-input-sm">Jenis</label>
                    <div class="col-sm-10">
                      <input type="text" name="jenis" class="form-control input-sm" required="" id="jenis" placeholder="jenis" value="<?php echo $data['jenis'];?>">
                    </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="form-group">
                    <label for="ruang" class="col-sm-2 control-label label-input-sm">Ruang</label>
                    <div class="col-sm-10">
                      <input type="text" name="ruang" class="form-control input-sm" required="" id="ruang" placeholder="ruang" value="<?php echo $data['ruang'];?>">
                    </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="form-group">
                    <label for="id_petugas" class="col-sm-2 control-label label-input-sm">Petugas</label>
                    <div class="col-sm-10">
                      <input type="text" name="id_petugas" class="form-control input-sm" required="" id="id_petugas" placeholder="id_petugas" value="<?php echo $data['id_petugas'];?>">
                    </div>
                  </div>
                  </div>
                  </form>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline">Simpan</button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>

          </div>

         



          <!-- /.control-sidebar -->
          <div class="control-sidebar-bg"></div>
        </div>
  <!-- ./wrapper -->
  <!-- jQuery 3 -->
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <script src="../bower_components/raphael/raphael.min.js"></script>
  <script src="../bower_components/morris.js/morris.min.js"></script>
  <!-- Sparkline -->
  <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="../bower_components/moment/min/moment.min.js"></script>
  <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="../bower_components/fastclick/lib/fastclick.js"></script>
  <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <script>
    $(function () {
      $('#example1').DataTable()
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
</body>
</html>
