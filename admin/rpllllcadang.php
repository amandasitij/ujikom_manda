<?php  include('head.php'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data barang
      <small>Preview</small>
    </h1>
    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#tambah">
            Tambah 
            data
          </button>
          <button class="btn btn-primary"><a href="export_excel_inventaris.php"><i class="fa fa-plus-circle"></i>Export to excel</button>
          <button class="btn btn-warning"><a href="lap_barang.php"><i class="fa fa-plus-circle"></i>Export to pdf</button>
          </a>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr class="info">
                <th>No</th>
                <th>Nama</th>
                <th>Kondisi</th>
                <th>Spesifikasi</th>
                <th>Keterangan</th>
                <th>Jumlah</th>
                <th>Jenis</th>
                <th>Tanggal Register</th>
                <th>Ruang</th>
                <th>Kode</th>
                <th>Petugas</th>
                <th>Option</th>
              </tr>
            </thead>

            <tbody>
              <?php
              include "../koneksi.php";
              if (isset($_GET['jurusan'])) {
                $bebas = $_GET['jurusan'];
                $pilih=mysql_query("SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis JOIN ruang r ON r.id_ruang = i.id_ruang JOIN petugas p ON p.id_petugas=i.id_petugas  WHERE nama_jenis='$bebas'");
              }
              else{
                $pilih=mysql_query("SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis JOIN ruang r ON r.id_ruang = i.id_ruang JOIN petugas p ON p.id_petugas=i.id_petugas");
              }
              $no=1;

              while ($data=mysql_fetch_array($pilih)) {
                ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?=$data['nama']; ?></td>
                  <td><?=$data['kondisi']; ?></td>
                  <td><?=$data['spesifikasi']; ?></td>
                  <td><?=$data['keterangan']; ?></td>
                  <td><?=$data['jumlah']; ?></td>
                  <td><?=$data['nama_jenis']; ?></td>
                  <td><?=$data['tanggal_register']; ?></td>
                  <td><?=$data['nama_ruang']; ?></td>
                  <td><?=$data['kode_inventaris']; ?></td>
                  <td><?=$data['nama_petugas']; ?></td>
                  <td>

                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#edit-<?php echo $data['id_inventaris'] ?>">
                      <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    </button>

                    <a onclick="return confirm('Apakah anda yakin ingin menghapus ini?')" class="hapus" href="hapus_rpl.php?id_inventaris=<?php echo $data['id_inventaris']; ?>">
                      <button type="button" class="btn btn-primary" aria-label="Left Align">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                      </button></a>

                    </td>
                  </tr>
                  <div class="modal modal-info fade" id="edit-<?php echo $data['id_inventaris'] ?>">
                    <div class="modal-dialog" style="width:470px;">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Edit barang</h4>
                          </div>
                          <div class="modal-body">
                            <form class="form-horizontal" action="edit_rpl.php" method="POST">                

                              <div class="row">
                               <div class="form-group">
                                <label for="nama" class="col-sm-2 control-label label-input-sm">Nama</label>
                                <div class="col-sm-10">
                                  <input type="hidden" name="id_inventaris" value="<?php echo $data['id_inventaris'] ?>">
                                  <input type="text"  name="nama" class="form-control input-sm" id="nama" placeholder="nama" value="<?php echo $data['nama'];?>">
                                </div>
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <div class="form-group">
                                <label for="kondisi" class="col-sm-2 control-label label-input-sm">Kondisi</label>
                                <div class="col-sm-10">
                                  <input type="text" name="kondisi" class="form-control input-sm" id="kondisi" placeholder="kondisi" value="<?php echo $data['kondisi'];?>">
                                </div>
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <div class="form-group">
                                <label for="spesifikasi" class="col-sm-2 control-label label-input-sm">Spesifikasi</label>
                                <div class="col-sm-10">
                                  <input type="text" name="spesifikasi" class="form-control input-sm" id="spesifikasi" placeholder="spesifikasi" value="<?php echo $data['spesifikasi'];?>">
                                </div>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                              <div class="form-group">
                                <label for="keterangan" class="col-sm-2 control-label label-input-sm">Keterangan</label>
                                <div class="col-sm-10">
                                  <input type="text" name="keterangan" class="form-control input-sm" id="keterangan" placeholder="keterangan" value="<?php echo $data['keterangan'];?>">
                                </div>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                              <div class="form-group">
                                <label for="jumlah" class="col-sm-2 control-label label-input-sm">Jumlah</label>
                                <div class="col-sm-10">
                                  <input type="text" name="jumlah" class="form-control input-sm" id="jumlah" placeholder="jumlah" value="<?php echo $data['jumlah'];?>" required>
                                </div>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                            <div class="form-group">
                              <label for="id_jenis" class="col-sm-2 control-label label-input-sm">Jenis</label>
                              <div class="col-sm-10">
                                <select name="id_jenis" class="from-control" style="width: 100%; color: black;" required="">

                                  <?php
                                  include"../koneksi.php";
                                  $sql=mysql_query("SELECT * FROM jenis");
                                  while ($data=mysql_fetch_array($sql)) {
                                    if ($data['id_jenis']==$tampil['id_jenis']){
                                      $selected="selected";
                                    }else{
                                      $selected="";
                                    }
                                    ?>
                                    <option value="<?php echo $data['id_jenis'];?>" <?php echo $selected?>><?php echo $data['nama_jenis'];?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <br>

                            <div class="row">
                            <div class="form-group">
                              <label for="ruang" class="col-sm-2 control-label label-input-sm">Ruang</label>
                              <div class="col-sm-10">
                                <select name="id_ruang" class="from-control" style="width: 100%; color: black;" required="">
                                  <?php
                                  include"../koneksi.php";
                                  $sql=mysql_query("SELECT * FROM ruang");
                                  while ($data=mysql_fetch_array($sql)) {
                                    if ($data['id_ruang']==$tampil['id_ruang']){
                                      $selected="selected";
                                    }else{
                                      $selected="";
                                    }
                                    ?>
                                    <option value="<?php echo $data['id_ruang'];?>" <?php echo $selected?>><?php echo $data['nama_ruang'];?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div> 
                          <br>
                          <div class="row">
                              <div class="form-group">
                                <label for="kode_inventaris" class="col-sm-2 control-label label-input-sm">Kode inventaris</label>
                                <div class="col-sm-10">
                                  <input type="text" name="kode_inventaris" class="form-control input-sm" id="kode_inventaris" placeholder="kode_inventaris" value="<?php echo $data['kode_inventaris'];?>" required>
                                </div>
                              </div>
                            </div>
                            <br>

                            <div class="row">
                            <div class="form-group">
                              <label for="petugas" class="col-sm-2 control-label label-input-sm">Petugas</label>
                              <div class="col-sm-10">
                                <select name="id_petugas" class="from-control" style="width: 100%; color: black;" required="">
                                  <?php
                                  include"../koneksi.php";
                                  $sql=mysql_query("SELECT * FROM petugas");
                                  while ($data=mysql_fetch_array($sql)) {
                                    if ($data['id_petugas']==$tampil['id_petugas']){
                                      $selected="selected";
                                    }else{
                                      $selected="";
                                    }
                                    ?>
                                    <option value="<?php echo $data['id_petugas'];?>" <?php echo $selected?>><?php echo $data['nama_petugas'];?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div><br>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-outline" name="edit">Simpan</button>
                            </div>
                          </div>
                          <!-- /.modal-content -->
                        </div>

                        <!-- /.modal-dialog -->
                      </div>

                    </div>
                  </form>
                  <?php
                  $no++;
                }
                ?>  
                <div class="modal modal-info fade" id="tambah">
                  <div class="modal-dialog" style="width:470px;">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Tambah data barang</h4>
                        </div>
                        <div class="modal-body">
                          <form class="form-horizontal" action="tambah_inventaris.php" method="POST">
                            <input type="hidden" name="id_inventaris" value="<?php echo $data['id'] ?>">
                            <div class="row">
                             <div class="form-group">
                              <label for="nama" class="col-sm-2 control-label label-input-sm">Nama</label>
                              <div class="col-sm-10">
                                <input type="text"  name="nama" class="form-control input-sm" required="" id="nama" placeholder="nama" value="" autocomplete="off">
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="form-group">
                              <label for="kondisi" class="col-sm-2 control-label label-input-sm">Kondisi</label>
                              <div class="col-sm-10">
                                <input type="text" name="kondisi" class="form-control input-sm" required="" autocomplete="off" id="kondisi" placeholder="kondisi" value="<?php echo $data['kondisi'];?>">
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="form-group">
                              <label for="spesifikasi" class="col-sm-2 control-label label-input-sm">Spesifikasi</label>
                              <div class="col-sm-10">
                                <input type="text" name="spesifikasi" class="form-control input-sm" required="" autocomplete="off" id="spesifikasi" placeholder="spesifikasi" value="<?php echo $data['spesifikasi'];?>">
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="form-group">
                              <label for="keterangan" class="col-sm-2 control-label label-input-sm">Keterangan</label>
                              <div class="col-sm-10">
                                <input type="text" name="keterangan1" class="form-control input-sm" required="" autocomplete="off" id="keterangan" placeholder="keterangan" value="<?php echo $data['keterangan'];?>">
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="form-group">
                              <label for="jumlah" class="col-sm-2 control-label label-input-sm">Jumlah</label>
                              <div class="col-sm-10">
                                <input type="text" name="jumlah" class="form-control input-sm" required="" id="jumlah" placeholder="jumlah" value="<?php echo $data['jumlah'];?>">
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="form-group">
                              <label for="id_jenis" class="col-sm-2 control-label label-input-sm">Jenis</label>
                              <div class="col-sm-10">
                                <select name="id_jenis" class="from-control" style="width: 100%; color: black;" required="">

                                  <?php
                                  include"../koneksi.php";
                                  $sql=mysql_query("SELECT * FROM jenis");
                                  while ($data=mysql_fetch_array($sql)) {
                                    if ($data['id_jenis']==$tampil['id_jenis']){
                                      $selected="selected";
                                    }else{
                                      $selected="";
                                    }
                                    ?>
                                    <option value="<?php echo $data['id_jenis'];?>" <?php echo $selected?>><?php echo $data['nama_jenis'];?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="form-group">
                              <label for="ruang" class="col-sm-2 control-label label-input-sm">Ruang</label>
                              <div class="col-sm-10">
                                <select name="id_ruang" class="from-control" style="width: 100%; color: black;" required="">
                                  <?php
                                  include"../koneksi.php";
                                  $sql=mysql_query("SELECT * FROM ruang");
                                  while ($data=mysql_fetch_array($sql)) {
                                    if ($data['id_ruang']==$tampil['id_ruang']){
                                      $selected="selected";
                                    }else{
                                      $selected="";
                                    }
                                    ?>
                                    <option value="<?php echo $data['id_ruang'];?>" <?php echo $selected?>><?php echo $data['nama_ruang'];?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div> 
                          <br>

                            <div class="row">
                            <div class="form-group">
                              <label for="kode_inventaris" class="col-sm-2 control-label label-input-sm">Kode inventaris</label>
                              <div class="col-sm-10">
                                <input type="text" name="kode_inventaris" class="form-control input-sm" required="" id="kode_inventaris" placeholder="kode_inventaris" value="<?php echo $data['kode_inventaris'];?>">
                              </div>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="form-group">
                              <label for="petugas" class="col-sm-2 control-label label-input-sm">Petugas</label>
                              <div class="col-sm-10">
                                <select name="id_petugas" class="from-control" style="width: 100%; color: black;" required="">
                                  <?php
                                  include"../koneksi.php";
                                  $sql=mysql_query("SELECT * FROM petugas");
                                  while ($data=mysql_fetch_array($sql)) {
                                    if ($data['id_petugas']==$tampil['id_petugas']){
                                      $selected="selected";
                                    }else{
                                      $selected="";
                                    }
                                    ?>
                                    <option value="<?php echo $data['id_petugas'];?>" <?php echo $selected?>><?php echo $data['nama_petugas'];?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div><br>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline" name="simpan">Simpan</button>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>

                  </div>

                </form>
                <div class="control-sidebar-bg"></div>
              </div>
            </section>
          </section>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->
        <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.7 -->
        <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
        <script src="../bower_components/raphael/raphael.min.js"></script>
        <script src="../bower_components/morris.js/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="../bower_components/moment/min/moment.min.js"></script>
        <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../bower_components/fastclick/lib/fastclick.js"></script>
        <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/adminlte.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="dist/js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="dist/js/demo.js"></script>
        <script>
          $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
          })
        </script>
      </body>
      </html>
      
