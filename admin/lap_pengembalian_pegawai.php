<?php
include '../koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 0038XXXXXXX',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. KIOS MALASNGODING',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.malasngoding.com email : malasngoding@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Pengembalian Pegawai",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Nama Pegawai', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Kode Pinjam', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Tanggal Pinjam', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Status Peminjaman', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Jumlah', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysql_query ("SELECT * FROM peminjaman2 pe LEFT JOIN detail_pinjam2 d ON pe.id_peminjaman2=d.id_peminjaman2 LEFT JOIN pegawai pg ON pe.id_pegawai=pg.id_pegawai LEFT JOIN inventaris i ON i.id_inventaris = d.id_inventaris");
       while ($lihat = mysql_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['nama_pegawai'],1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['kode_peminjaman2'], 1, 0,'C');
	$pdf->Cell(5, 0.8, $lihat['tanggal_pinjam'],1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['status_peminjaman2'], 1, 0,'C');
	$pdf->Cell(5, 0.8, $lihat['jumlahp2'], 1, 0,'C');


	$no++;
}

$pdf->Output("laporan_pengembalian_pegawai.pdf","I");

?>


