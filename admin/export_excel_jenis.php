n<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
  </style>

<?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Jenis.xls");
?>

<center>
  <h1>Data Jenis</h1>
</center>

<table border="1">
 <thead>
    <tr>
    	<th>#</th>
		<th>Nama Jenis</th>
   	 	<th>Kode Jenis</th>
    	<th>Keterangan</th>
    </tr>
    </thead>
    <tbody>
	<?php
	    include "../koneksi.php";
	    $no=1;
	    $select = mysql_query ("SELECT * FROM jenis");
	    	while ($data = mysql_fetch_array($select)) {
    ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td><?=$data['nama_jenis']; ?></td>
        <td><?=$data['kode_jenis']; ?></td>
        <td><?=$data['keterangan_jenis']; ?></td>		
    </tr>
	<?php
		}
	?>
    </tbody>
</table>
                                 
</body>
</html>