<?php  include('head.php'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Ruangan
      <small>Preview</small>
    </h1>
    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#tambah">
                Tambah 
                data
              </button>
              <button class="btn btn-primary"><a href="export_excel_ruang.php"><i class="fa fa-plus-circle"></i>Export to excel</button>
          <button class="btn btn-warning"><a href="lap_ruang.php"><i class="fa fa-plus-circle"></i>Export to pdf</button></a>
                      </div>

      <!-- /.box-header -->
       <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr class="info">
              <th>No</th>
              <th>Nama Ruang</th>
              <th>Kode Ruang</th>
              <th>Keterangan</th>
              <th>OPSI</th>
            </tr>
          </thead>

          <tbody>
            <?php
            include "../koneksi.php";
            $no=1;
            $pilih=mysql_query("select * from ruang");
            while ($data=mysql_fetch_array($pilih)) {
              ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?=$data['nama_ruang']; ?></td>
                <td><?=$data['kode_ruang']; ?></td>
                <td><?=$data['keterangan_ruang']; ?></td>
                <td>

                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#edit-<?php echo $data['id_ruang'] ?>">
                      <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    </button></a>

                    <a onclick="return confirm('Apakah anda yakin ingin menghapus ini?')" class="hapus" href="hapus_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>">
                      <button type="button" class="btn btn-primary" aria-label="Left Align">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                      </button></a>
                    </td>
                  </tr>
                  
          <div class="modal modal-info fade" id="edit-<?php echo $data['id_ruang'] ?>">
          <div class="modal-dialog" style="width:470px;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit </h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" action="edit_ruang.php" method="POST">
                
                <div class="row">
            <div class="form-group">
            <input type="hidden" name="id_ruang" value="<?php echo $data['id_ruang'] ?>">
                <label for="nama_ruang" class="col-sm-2 control-label label-input-sm">Nama Ruang</label>
                <div class="col-sm-10">
                    <input type="text"  name="nama_ruang" class="form-control input-sm" id="nama_ruang" placeholder="nama_ruang" value="<?php echo $data['nama_ruang'];?>">
                </div>
            </div>
            </div>
            <div class="row">
            <div class="form-group">
                <label for="kode_ruang" class="col-sm-2 control-label label-input-sm">Kode Ruang</label>
                <div class="col-sm-10">
                    <input type="text" name="kode_ruang" class="form-control input-sm" id="kode_ruang" placeholder="kode_ruang" value="<?php echo $data['kode_ruang'];?>">
                </div>
              </div>
              </div>
              <div class="row">
              <div class="form-group">
                <label for="keterangan_ruang" class="col-sm-2 control-label label-input-sm">Keterangan</label>
                <div class="col-sm-10">
                    <input type="text" name="keterangan_ruang" class="form-control input-sm" id="keterangan_ruang" placeholder="keterangan_ruang" value="<?php echo $data['keterangan_ruang'];?>">
                </div>
              </div>
              </div>
              <br>
              <div class="row">
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-outline" name="edit">Simpan</button>
              </div>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

          </div>
                  <?php
                  $no++;
                }
                ?>  
              </tbody>
            </table>

<div class="modal modal-info fade" id="tambah">
          <div class="modal-dialog" style="width:470px;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah data ruang</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" action="tambah_ruang.php" method="POST">
                <input type="hidden" name="id_ruang" value="<?php echo $data['id'] ?>">
         <div class="form-group">
                <label for="nama_ruang" class="col-sm-2 control-label label-input-sm">Nama Ruang </label>
                <div class="col-sm-10">
                    <input type="text"  name="nama_ruang" class="form-control input-sm" required="" id="nama_ruang" placeholder="nama_ruang" value="<?php echo $data['nama_ruang'];?>">
                </div>
            </div>
            <div class="form-group">
                <label for="kode_ruang" class="col-sm-2 control-label label-input-sm">Kode Ruang</label>
                <div class="col-sm-10">
                    <input type="text" name="kode_ruang" class="form-control input-sm" required="" id="kode_ruang" placeholder="kode_ruang" value="<?php echo $data['kode_ruang'];?>">
                </div>
              </div>
              <div class="form-group">
                <label for="keterangan_ruang" class="col-sm-2 control-label label-input-sm">Keterangan</label>
                <div class="col-sm-10">
                    <input type="text" name="keterangan_ruang" class="form-control input-sm" required="" id="keterangan_ruang" placeholder="keterangan_ruang" value="<?php echo $data['keterangan_ruang'];?>">
                </div>
              </div>
              
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-outline" name="simpan">Simpan</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

          </div>
          </form>
          
          
          <!-- /.control-sidebar -->
        <div class="control-sidebar-bg"></div>
      </div>
    </section>
  </section>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../bower_components/raphael/raphael.min.js"></script>
<script src="../bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../bower_components/moment/min/moment.min.js"></script>
<script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../bower_components/fastclick/lib/fastclick.js"></script>
<script src="../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
