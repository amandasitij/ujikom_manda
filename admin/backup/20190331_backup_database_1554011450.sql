DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(20) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp` varchar(30) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("2","KD1942Mar","42","1","0");
INSERT INTO detail_pinjam VALUES("3","KD1946Mar","46","1","0");
INSERT INTO detail_pinjam VALUES("4","KD1946Mar","46","1","0");
INSERT INTO detail_pinjam VALUES("5","KD1946Mar","46","1","11");
INSERT INTO detail_pinjam VALUES("6","KD1946Mar","46","1","12");
INSERT INTO detail_pinjam VALUES("7","KD1942Mar","42","1","13");
INSERT INTO detail_pinjam VALUES("8","KD1942Mar","42","2","14");
INSERT INTO detail_pinjam VALUES("9","KD1942Mar","42","1","15");
INSERT INTO detail_pinjam VALUES("10","KD1942Mar","42","1","16");
INSERT INTO detail_pinjam VALUES("11","KD1942Mar","42","1","17");
INSERT INTO detail_pinjam VALUES("12","KD1946Mar","46","40","18");
INSERT INTO detail_pinjam VALUES("13","KD1946Mar","46","90","19");
INSERT INTO detail_pinjam VALUES("14","KD1956Mar","56","1","20");
INSERT INTO detail_pinjam VALUES("15","KD1943Mar","43","1","21");
INSERT INTO detail_pinjam VALUES("16","KD1942Mar","42","1","22");
INSERT INTO detail_pinjam VALUES("17","KD1942Mar","42","1","23");
INSERT INTO detail_pinjam VALUES("18","KD1946Mar","46","85","24");
INSERT INTO detail_pinjam VALUES("19","KD1942Mar","42","1","25");
INSERT INTO detail_pinjam VALUES("20","KD1943Mar","43","1","26");
INSERT INTO detail_pinjam VALUES("21","KD1942Mar","42","1","27");
INSERT INTO detail_pinjam VALUES("22","KD1946Mar","46","2","28");
INSERT INTO detail_pinjam VALUES("23","KD1942Mar","42","1","29");
INSERT INTO detail_pinjam VALUES("24","KD1945Mar","45","1","30");
INSERT INTO detail_pinjam VALUES("25","KD1942Mar","42","1","31");
INSERT INTO detail_pinjam VALUES("26","KD1942Mar","42","1","32");
INSERT INTO detail_pinjam VALUES("27","KD1942Mar","42","1","33");
INSERT INTO detail_pinjam VALUES("28","KD1942Mar","42","4","34");
INSERT INTO detail_pinjam VALUES("29","KD1943Mar","43","1","35");
INSERT INTO detail_pinjam VALUES("30","KD1943Mar","43","5","36");
INSERT INTO detail_pinjam VALUES("31","KD1944Mar","44","1","37");
INSERT INTO detail_pinjam VALUES("32","KD1944Mar","44","1","38");
INSERT INTO detail_pinjam VALUES("33","KD1943Mar","43","1","39");
INSERT INTO detail_pinjam VALUES("34","KD1944Mar","44","4","40");
INSERT INTO detail_pinjam VALUES("35","KD1945Mar","45","1","41");
INSERT INTO detail_pinjam VALUES("36","KD1946Mar","46","1","42");
INSERT INTO detail_pinjam VALUES("37","KD1942Mar","42","1","43");
INSERT INTO detail_pinjam VALUES("38","KD1942Mar","42","1","44");
INSERT INTO detail_pinjam VALUES("39","KD1942Mar","42","1","45");
INSERT INTO detail_pinjam VALUES("40","KD1942Mar","42","1","46");
INSERT INTO detail_pinjam VALUES("41","KD1942Mar","42","1","47");
INSERT INTO detail_pinjam VALUES("42","KD1944Mar","44","1","48");
INSERT INTO detail_pinjam VALUES("43","KD1942Mar","42","1","49");
INSERT INTO detail_pinjam VALUES("44","KD1945Mar","45","1","50");
INSERT INTO detail_pinjam VALUES("45","KD1944Mar","44","1","51");
INSERT INTO detail_pinjam VALUES("46","KD1944Mar","44","1","52");
INSERT INTO detail_pinjam VALUES("47","KD1943Mar","43","1","53");
INSERT INTO detail_pinjam VALUES("48","KD1943Mar","43","1","54");
INSERT INTO detail_pinjam VALUES("49","KD1943Mar","43","1","55");
INSERT INTO detail_pinjam VALUES("50","KD1943Mar","43","1","56");
INSERT INTO detail_pinjam VALUES("51","KD1943Mar","43","1","57");
INSERT INTO detail_pinjam VALUES("52","KD1943Mar","43","1","58");
INSERT INTO detail_pinjam VALUES("53","KD1943Mar","43","1","59");
INSERT INTO detail_pinjam VALUES("54","KD1943Mar","43","1","60");
INSERT INTO detail_pinjam VALUES("55","KD1943Mar","43","1","61");
INSERT INTO detail_pinjam VALUES("56","KD1943Mar","43","1","62");
INSERT INTO detail_pinjam VALUES("57","KD1943Mar","43","1","63");
INSERT INTO detail_pinjam VALUES("58","KD1943Mar","43","1","64");
INSERT INTO detail_pinjam VALUES("59","KD1943Mar","43","1","65");
INSERT INTO detail_pinjam VALUES("60","KD1943Mar","43","1","66");
INSERT INTO detail_pinjam VALUES("61","KD1943Mar","43","1","67");
INSERT INTO detail_pinjam VALUES("62","KD1943Mar","43","1","68");
INSERT INTO detail_pinjam VALUES("63","KD1949Mar","49","1","69");
INSERT INTO detail_pinjam VALUES("64","KD1945Mar","45","1","70");



DROP TABLE detail_pinjam2;

CREATE TABLE `detail_pinjam2` (
  `id_detail_pinjam2` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman_detail2` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp2` varchar(25) NOT NULL,
  `id_peminjaman2` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam2`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam2 VALUES("1","","42","","1","0");
INSERT INTO detail_pinjam2 VALUES("2","","42","","2","0");
INSERT INTO detail_pinjam2 VALUES("3","KD1942Mar","42","","3","0");
INSERT INTO detail_pinjam2 VALUES("4","KD1942Mar","42","1","4","0");
INSERT INTO detail_pinjam2 VALUES("5","KD1942Mar","42","1","5","0");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `spesifikasi` varchar(50) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` varchar(30) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` varchar(30) NOT NULL,
  `kode_inventaris` varchar(30) NOT NULL,
  `id_petugas` varchar(30) NOT NULL,
  `sumber` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("43","Harddisk","New","","Baik","13","6","2019-02-26 03:51:03","1","2222","1","","");
INSERT INTO inventaris VALUES("44","Kamera","Lama","","Baik","3","7","2019-02-26 21:11:08","2","11222","2","","");
INSERT INTO inventaris VALUES("45","Mic","New","","Baik","4","7","2019-02-26 01:03:03","2","1149809","2","","");
INSERT INTO inventaris VALUES("46","Obeng","Baru","","baik","167","8","2019-02-26 10:16:12","1","","2","","");
INSERT INTO inventaris VALUES("47","Machine","New","","baik","15","8","2019-02-26 03:23:40","1","68758","1","","");
INSERT INTO inventaris VALUES("48","Helm pelindung","New","","Baik","20","9","2019-02-26 04:08:22","2","8986","2","","");
INSERT INTO inventaris VALUES("49","Mesin","New","","baik","12","9","2019-02-26 03:11:24","1","6428","1","","");
INSERT INTO inventaris VALUES("50","Komputer","New","","Baik","38","10","2019-02-26 03:14:17","2","789969","1","","");
INSERT INTO inventaris VALUES("51","CPU","Lama","","Baik","38","10","2019-02-26 03:09:15","1","895674","2","","");
INSERT INTO inventaris VALUES("52","Processor","New","","Baik","18","11","2019-02-26 04:10:13","2","8911911","1","","");
INSERT INTO inventaris VALUES("53","Speaker","Lama","","Baik","10","11","2019-02-26 07:08:09","1","291010","2","","");
INSERT INTO inventaris VALUES("54","CD-RW","baik","","baru","15","6","2019-03-08 14:40:09","1","V0001","1","","");
INSERT INTO inventaris VALUES("55","Laptop-002","Baik","","Lemot","1","6","2019-03-08 02:08:14","1","00278","1","","");
INSERT INTO inventaris VALUES("56","Laptop-003","Baik","","Kenceng","1","6","2019-03-08 06:17:08","2","090909","1","","");
INSERT INTO inventaris VALUES("57","lepi-004","baik","","ok","9","6","2019-03-11 14:32:49","1","V0001","1","","");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(25) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("6","Rekayasa perangkat lunak","001","baik");
INSERT INTO jenis VALUES("7","Broadcasting","002","baik");
INSERT INTO jenis VALUES("8","Teknik kendaraan ringan","003","baik");
INSERT INTO jenis VALUES("9","Teknik pengelasan","004","baik");
INSERT INTO jenis VALUES("10","Animasi","005","baik");
INSERT INTO jenis VALUES("11","Lainnya","006","baik");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","user");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` int(11) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("3","wiwi","3546758","semplak");
INSERT INTO pegawai VALUES("4","manda","789179","kaum");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `nama_peminjam` varchar(50) NOT NULL,
  `kode_peminjaman` varchar(20) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("67","Om dahlan","KD1943Mar","2019-03-29 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","43","1","0");
INSERT INTO peminjaman VALUES("68","Om dahlan","KD1943Mar","2019-03-29 00:00:00","2019-03-29 14:09:16","Dikembalikan","43","1","0");
INSERT INTO peminjaman VALUES("69","abi","KD1949Mar","2019-03-29 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","49","1","0");
INSERT INTO peminjaman VALUES("70","aman","KD1945Mar","2019-03-30 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","45","1","0");



DROP TABLE peminjaman2;

CREATE TABLE `peminjaman2` (
  `id_peminjaman2` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman2` varchar(30) NOT NULL,
  `nama_barang` varchar(25) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman2` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman2`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman2 VALUES("1","KD1942Mar","","2019-03-29 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","0","0");
INSERT INTO peminjaman2 VALUES("2","KD1942Mar","","2019-03-29 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","0","3");
INSERT INTO peminjaman2 VALUES("3","KD1942Mar","","2019-03-29 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","0","3");
INSERT INTO peminjaman2 VALUES("4","KD1942Mar","","2019-03-29 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","0","3");
INSERT INTO peminjaman2 VALUES("5","KD1942Mar","","2019-03-29 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","42","3");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('Yes','No') NOT NULL,
  `logintime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin1","admin1","Amanda siti jubaedah","1","No","0");
INSERT INTO petugas VALUES("2","op1","operator","Siti","2","No","0");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` varchar(25) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Teori-001","001","baiks");
INSERT INTO ruang VALUES("2","Teori-002","002","baik");



