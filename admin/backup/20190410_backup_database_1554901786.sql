DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(20) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp` varchar(30) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("84","KP-FDD1BEJ60P","76","1","90");



DROP TABLE detail_pinjam2;

CREATE TABLE `detail_pinjam2` (
  `id_detail_pinjam2` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman_detail2` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp2` varchar(25) NOT NULL,
  `id_peminjaman2` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam2`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam2 VALUES("16","KP-T64IFISP2D","78","1","17","0");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `spesifikasi` varchar(50) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` varchar(30) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` varchar(30) NOT NULL,
  `kode_inventaris` varchar(30) NOT NULL,
  `id_petugas` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("76","Laptop-001","Baru","Acer","Baik","19","6","2019-04-10 03:23:04","1","KD-001","1","");
INSERT INTO inventaris VALUES("77","Kamera","Baru","Canon","Baik","5","7","2019-04-10 02:12:07","2","KD-002","1","");
INSERT INTO inventaris VALUES("78","Obeng","Lama","Azu 1 set","Baik","12","8","2019-04-10 02:27:48","2","KD-003","1","");
INSERT INTO inventaris VALUES("79","Helm pelindung","Baru","Chin","Baik","10","9","2019-04-10 07:11:08","2","KD-004","1","");
INSERT INTO inventaris VALUES("80","Komp-001","Lama","Lenovo","Baik","25","10","2019-04-10 03:09:23","1","KD-005","1","");
INSERT INTO inventaris VALUES("81","Infocus","Baru","BenQ","Baik","15","11","2019-04-10 08:10:36","1","KD-006","1","");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(25) NOT NULL,
  `keterangan_jenis` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("6","RPL","001","baik");
INSERT INTO jenis VALUES("7","BC","002","baik");
INSERT INTO jenis VALUES("8","TKR","003","baik");
INSERT INTO jenis VALUES("9","TPL","004","baik");
INSERT INTO jenis VALUES("10","Animasi","005","baik");
INSERT INTO jenis VALUES("11","Lainnya","006","baik");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","user");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` int(11) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("3","wiwi","3546758","semplak");
INSERT INTO pegawai VALUES("4","manda","789179","kaum");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `nama_peminjam` varchar(50) NOT NULL,
  `kode_peminjaman` varchar(20) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("90","Rheina","KP-FDD1BEJ60P","2019-04-10 10:58:07","0000-00-00 00:00:00","Sedang di pinjam","76","3","0");



DROP TABLE peminjaman2;

CREATE TABLE `peminjaman2` (
  `id_peminjaman2` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman2` varchar(30) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman2` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman2`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman2 VALUES("17","KP-T64IFISP2D","2019-04-10 00:00:00","2019-04-10 11:26:53","Dikembalikan","78","4");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin1","admin1","Amanda","amandasitij@gmail.com","1");
INSERT INTO petugas VALUES("2","op1","operator","Siti","amandasitij@gmail.com","2");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` varchar(25) NOT NULL,
  `keterangan_ruang` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Teori-001","001","baikszzzzzzz");
INSERT INTO ruang VALUES("2","Teori-002","002","baik");



