DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(20) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp` varchar(30) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("2","KD1942Mar","42","1","0");
INSERT INTO detail_pinjam VALUES("3","KD1946Mar","46","1","0");
INSERT INTO detail_pinjam VALUES("4","KD1946Mar","46","1","0");
INSERT INTO detail_pinjam VALUES("5","KD1946Mar","46","1","11");
INSERT INTO detail_pinjam VALUES("6","KD1946Mar","46","1","12");
INSERT INTO detail_pinjam VALUES("7","KD1942Mar","42","1","13");
INSERT INTO detail_pinjam VALUES("8","KD1942Mar","42","2","14");
INSERT INTO detail_pinjam VALUES("9","KD1942Mar","42","1","15");
INSERT INTO detail_pinjam VALUES("10","KD1942Mar","42","1","16");
INSERT INTO detail_pinjam VALUES("11","KD1942Mar","42","1","17");
INSERT INTO detail_pinjam VALUES("12","KD1946Mar","46","40","18");
INSERT INTO detail_pinjam VALUES("13","KD1946Mar","46","90","19");
INSERT INTO detail_pinjam VALUES("14","KD1956Mar","56","1","20");
INSERT INTO detail_pinjam VALUES("15","KD1943Mar","43","1","21");
INSERT INTO detail_pinjam VALUES("16","KD1942Mar","42","1","22");
INSERT INTO detail_pinjam VALUES("17","KD1942Mar","42","1","23");
INSERT INTO detail_pinjam VALUES("18","KD1946Mar","46","85","24");
INSERT INTO detail_pinjam VALUES("19","KD1942Mar","42","1","25");
INSERT INTO detail_pinjam VALUES("20","KD1943Mar","43","1","26");
INSERT INTO detail_pinjam VALUES("21","KD1942Mar","42","1","27");
INSERT INTO detail_pinjam VALUES("22","KD1946Mar","46","2","28");
INSERT INTO detail_pinjam VALUES("23","KD1942Mar","42","1","29");
INSERT INTO detail_pinjam VALUES("24","KD1945Mar","45","1","30");
INSERT INTO detail_pinjam VALUES("25","KD1942Mar","42","1","31");
INSERT INTO detail_pinjam VALUES("26","KD1942Mar","42","1","32");
INSERT INTO detail_pinjam VALUES("27","KD1942Mar","42","1","33");
INSERT INTO detail_pinjam VALUES("28","KD1942Mar","42","4","34");
INSERT INTO detail_pinjam VALUES("29","KD1943Mar","43","1","35");
INSERT INTO detail_pinjam VALUES("30","KD1943Mar","43","5","36");
INSERT INTO detail_pinjam VALUES("31","KD1944Mar","44","1","37");
INSERT INTO detail_pinjam VALUES("32","KD1944Mar","44","1","38");
INSERT INTO detail_pinjam VALUES("33","KD1943Mar","43","1","39");
INSERT INTO detail_pinjam VALUES("34","KD1944Mar","44","4","40");
INSERT INTO detail_pinjam VALUES("35","KD1945Mar","45","1","41");
INSERT INTO detail_pinjam VALUES("36","KD1946Mar","46","1","42");
INSERT INTO detail_pinjam VALUES("37","KD1942Mar","42","1","43");
INSERT INTO detail_pinjam VALUES("38","KD1942Mar","42","1","44");
INSERT INTO detail_pinjam VALUES("39","KD1942Mar","42","1","45");
INSERT INTO detail_pinjam VALUES("40","KD1942Mar","42","1","46");
INSERT INTO detail_pinjam VALUES("41","KD1942Mar","42","1","47");
INSERT INTO detail_pinjam VALUES("42","KD1944Mar","44","1","48");
INSERT INTO detail_pinjam VALUES("43","KD1942Mar","42","1","49");
INSERT INTO detail_pinjam VALUES("44","KD1945Mar","45","1","50");
INSERT INTO detail_pinjam VALUES("45","KD1944Mar","44","1","51");
INSERT INTO detail_pinjam VALUES("46","KD1944Mar","44","1","52");
INSERT INTO detail_pinjam VALUES("47","KD1943Mar","43","1","53");
INSERT INTO detail_pinjam VALUES("48","KD1943Mar","43","1","54");
INSERT INTO detail_pinjam VALUES("49","KD1943Mar","43","1","55");
INSERT INTO detail_pinjam VALUES("50","KD1943Mar","43","1","56");
INSERT INTO detail_pinjam VALUES("51","KD1943Mar","43","1","57");
INSERT INTO detail_pinjam VALUES("52","KD1943Mar","43","1","58");
INSERT INTO detail_pinjam VALUES("53","KD1943Mar","43","1","59");
INSERT INTO detail_pinjam VALUES("54","KD1943Mar","43","1","60");
INSERT INTO detail_pinjam VALUES("55","KD1943Mar","43","1","61");
INSERT INTO detail_pinjam VALUES("56","KD1943Mar","43","1","62");
INSERT INTO detail_pinjam VALUES("57","KD1943Mar","43","1","63");
INSERT INTO detail_pinjam VALUES("58","KD1943Mar","43","1","64");
INSERT INTO detail_pinjam VALUES("59","KD1943Mar","43","1","65");
INSERT INTO detail_pinjam VALUES("60","KD1943Mar","43","1","66");
INSERT INTO detail_pinjam VALUES("61","KD1943Mar","43","1","67");
INSERT INTO detail_pinjam VALUES("62","KD1943Mar","43","1","68");
INSERT INTO detail_pinjam VALUES("63","KD1949Mar","49","1","69");
INSERT INTO detail_pinjam VALUES("64","KD1945Mar","45","1","70");
INSERT INTO detail_pinjam VALUES("65","KD1943Apr","43","2","71");
INSERT INTO detail_pinjam VALUES("66","KD1943Apr","43","1","72");
INSERT INTO detail_pinjam VALUES("67","","43","1","73");
INSERT INTO detail_pinjam VALUES("68","","43","1","74");
INSERT INTO detail_pinjam VALUES("69","","43","1","75");
INSERT INTO detail_pinjam VALUES("70","","43","1","76");
INSERT INTO detail_pinjam VALUES("71","PJM1943Apr","43","1","77");
INSERT INTO detail_pinjam VALUES("72","PJM1946Apr","46","2","78");
INSERT INTO detail_pinjam VALUES("73","PJM1943Apr","43","1","79");
INSERT INTO detail_pinjam VALUES("74","PJM1946Apr","46","1","80");
INSERT INTO detail_pinjam VALUES("75","KP-B4NBSNPTUL","43","1","81");
INSERT INTO detail_pinjam VALUES("76","KP-61BTSRE0S1","44","1","82");
INSERT INTO detail_pinjam VALUES("77","KP-A869L8PIJE","44","1","83");
INSERT INTO detail_pinjam VALUES("78","KP-SM7NSSEU5Q","43","1","84");
INSERT INTO detail_pinjam VALUES("79","KP-U9FM6G4Q1H","71","1","85");
INSERT INTO detail_pinjam VALUES("80","KP-PNIALP3A1J","71","1","86");
INSERT INTO detail_pinjam VALUES("81","KP-K09IK7LHH8","72","1","87");
INSERT INTO detail_pinjam VALUES("82","KP-BLK2PKD2BA","71","1","88");



DROP TABLE detail_pinjam2;

CREATE TABLE `detail_pinjam2` (
  `id_detail_pinjam2` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman_detail2` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp2` varchar(25) NOT NULL,
  `id_peminjaman2` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam2`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam2 VALUES("10","KD1943Apr","43","3","10","0");
INSERT INTO detail_pinjam2 VALUES("11","KP-RT2D6JKR5G","71","1","12","0");
INSERT INTO detail_pinjam2 VALUES("12","KP-TLUURD3PC7","74","1","13","0");
INSERT INTO detail_pinjam2 VALUES("13","KP-J6PEM7HIUG","72","1","14","0");
INSERT INTO detail_pinjam2 VALUES("14","KP-SNT6IAPDCN","72","1","15","0");
INSERT INTO detail_pinjam2 VALUES("15","KP-E9HNT7LNHC","72","1","16","0");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `spesifikasi` varchar(50) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` varchar(30) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` varchar(30) NOT NULL,
  `kode_inventaris` varchar(30) NOT NULL,
  `id_petugas` varchar(30) NOT NULL,
  `sumber` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("71","hard disk","baik","vb","baiks","2","6","2019-04-06 17:09:19","1","","1","","");
INSERT INTO inventaris VALUES("72","laptop","higi","uhui","baiks","24","6","2019-04-06 17:19:14","1","","1","","");
INSERT INTO inventaris VALUES("73","iyaakss","huhu","apaa","crystal","2","","2019-04-07 06:10:39","1","","1","","");
INSERT INTO inventaris VALUES("74","blaa","hhh","jjj","yyyyyyzzz","3","6","2019-04-07 06:23:55","1","","1","","");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(25) NOT NULL,
  `keterangan_jenis` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("6","RPL","001","baik");
INSERT INTO jenis VALUES("7","BC","002","baik");
INSERT INTO jenis VALUES("8","TKR","003","baik");
INSERT INTO jenis VALUES("9","TPL","004","baik");
INSERT INTO jenis VALUES("10","Animasi","005","baik");
INSERT INTO jenis VALUES("11","Lainnya","006","baik");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","user");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` int(11) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("3","wiwi","3546758","semplak");
INSERT INTO pegawai VALUES("4","manda","789179","kaum");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `nama_peminjam` varchar(50) NOT NULL,
  `kode_peminjaman` varchar(20) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("78","AmanTerkendali","PJM1946Apr","2019-04-01 00:00:00","2019-04-01 12:24:10","Dikembalikan","46","1","0");
INSERT INTO peminjaman VALUES("79","zz","PJM1943Apr","2019-04-01 12:28:23","2019-04-01 12:29:07","Dikembalikan","43","1","0");
INSERT INTO peminjaman VALUES("80","nnn","PJM1946Apr","2019-04-01 12:29:42","2019-04-04 08:30:26","Dikembalikan","46","1","0");
INSERT INTO peminjaman VALUES("81","","KP-B4NBSNPTUL","2019-04-04 08:43:29","2019-04-04 08:52:30","Dikembalikan","43","1","0");
INSERT INTO peminjaman VALUES("82","mans","KP-61BTSRE0S1","2019-04-04 08:50:42","2019-04-04 08:50:53","Dikembalikan","44","1","0");
INSERT INTO peminjaman VALUES("83","jj","KP-A869L8PIJE","2019-04-04 08:52:47","2019-04-04 08:52:56","Dikembalikan","44","1","0");
INSERT INTO peminjaman VALUES("84","hhh","KP-SM7NSSEU5Q","2019-04-04 13:48:10","2019-04-07 08:23:46","Dikembalikan","43","1","0");
INSERT INTO peminjaman VALUES("85","mas","KP-U9FM6G4Q1H","2019-04-07 06:26:30","2019-04-07 06:28:06","Dikembalikan","71","1","0");
INSERT INTO peminjaman VALUES("86","balaalla","KP-PNIALP3A1J","2019-04-07 06:31:26","2019-04-07 07:29:45","Dikembalikan","71","0","0");
INSERT INTO peminjaman VALUES("87","hiyaaaa","KP-K09IK7LHH8","2019-04-07 06:33:45","2019-04-07 06:33:57","Dikembalikan","72","0","0");
INSERT INTO peminjaman VALUES("88","gh","KP-BLK2PKD2BA","2019-04-07 08:19:22","0000-00-00 00:00:00","Sedang di pinjam","71","0","0");



DROP TABLE peminjaman2;

CREATE TABLE `peminjaman2` (
  `id_peminjaman2` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman2` varchar(30) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman2` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman2`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman2 VALUES("10","KD1943Apr","2019-04-05 00:00:00","2019-04-07 10:19:25","Dikembalikan","43","4");
INSERT INTO peminjaman2 VALUES("11","KP-9RMPR23LUNdasadadasdasadads","2019-04-07 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","74","3");
INSERT INTO peminjaman2 VALUES("12","KP-RT2D6JKR5G","2019-04-07 00:00:00","2019-04-07 10:19:37","Dikembalikan","71","3");
INSERT INTO peminjaman2 VALUES("13","KP-TLUURD3PC7","2019-04-07 00:00:00","2019-04-07 10:20:11","Dikembalikan","74","3");
INSERT INTO peminjaman2 VALUES("14","KP-J6PEM7HIUG","2019-04-07 00:00:00","2019-04-07 10:20:32","Dikembalikan","72","3");
INSERT INTO peminjaman2 VALUES("15","KP-SNT6IAPDCN","2019-04-07 00:00:00","2019-04-07 10:20:36","Dikembalikan","72","4");
INSERT INTO peminjaman2 VALUES("16","KP-E9HNT7LNHC","2019-04-07 00:00:00","0000-00-00 00:00:00","Sedang di pinjam","72","4");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('Yes','No') NOT NULL,
  `logintime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin1","admin1","Amanda","","1","No","0");
INSERT INTO petugas VALUES("2","op1","operator","Siti","","2","No","0");
INSERT INTO petugas VALUES("3","aman01","fd25Ahj1","aman","amandasitij@gmail.com","1","","");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` varchar(25) NOT NULL,
  `keterangan_ruang` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Teori-001","001","baikszzzzzzz");
INSERT INTO ruang VALUES("2","Teori-002","002","baik");



