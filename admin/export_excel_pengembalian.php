n<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
  </style>

<?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data pengembalian admin.xls");
?>

<center>
  <h1>Data pengembalian admin/operator</h1>
</center>

<table border="1">
 <thead>
    <tr>
    	<th>#</th>
		 <th>Nama Peminjam</th>
            <th>Kode Peminjam</th>
            <th>Tanggal Pinjam</th>
            <th>Status Peminjaman</th>
    </tr>
    </thead>
    <tbody>
	<?php
	    include "../koneksi.php";
	    $no=1;
	    $select = mysql_query ("SELECT * FROM peminjaman p LEFT JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman LEFT JOIN petugas pt ON p.id_petugas=pt.id_petugas LEFT JOIN inventaris i ON  i.id_inventaris = d.id_inventaris LEFT JOIN pegawai peg ON peg.id_pegawai=p.id_pegawai order by status_peminjaman");
	    	while ($data = mysql_fetch_array($select)) {
    ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td class='text-center'><?=$data['nama_peminjam']?></td>
          <td class='text-center'><?=$data['kode_peminjaman']?></td>
          <td class='text-center'><?=$data['tanggal_pinjam']?></td>
          <td class='text-center'><?=$data['status_peminjaman']?></td>
    </tr>
	<?php
		}
	?>
    </tbody>
</table>
                                 
</body>
</html>