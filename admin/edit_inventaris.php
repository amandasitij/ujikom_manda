<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ADMIN INVENSE</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body style="background-color: #dadee2;">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="ruang.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b>INVENSE</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
              </a>

              <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">

                    <!-- end message -->
                    <li>
                      <a href="#">
                        <div class="pull-left">
                          <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          AdminLTE Design Team
                          <small><i class="fa fa-clock-o"></i> 2 hours</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <div class="pull-left">
                          <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          Developers
                          <small><i class="fa fa-clock-o"></i> Today</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>

                    <li>

                    </li>
                  </ul>
                </li>

              </ul>
            </li>
            <li class="dropdown">
              <a href="../index.php"><i class="glyphicon glyphicon-log-out"></i></a>
            </li>
            <!-- Notifications: style can be found in dropdown.less -->

            <!-- User Account: style can be found in dropdown.less -->


            <!-- Control Sidebar Toggle Button -->

          </ul>
        </div>
      </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image" style="    max-width: 181px;">
            <img src="images/onye.png" width="150px" height="130px" class="img-circle" alt="User Image">
          </div>

        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MAIN NAVIGATION</li>
          <li class="treeview">
            <a href="?page=index_admin">
              <i class="fa  fa-home"></i> <span>Inventarisir</span>
              <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
              </span>
              </a>
              <ul class="treeview-menu">
      <li><a href="rpl.php"><i class="fa fa-circle-o-notch"></i> RPL</a></li>
      <li><a href="animasi.php"><i class="fa fa-circle-o-notch"></i> ANIMASI</a></li>
      <li><a href="broadcast.php"><i class="fa fa-circle-o-notch"></i> BROADCASTING</a></li>
      <li><a href="tkr.php"><i class="fa fa-circle-o-notch"></i> TKR</a></li>
      <li><a href="tpl.php"><i class="fa fa-circle-o-notch"></i> TPL</a></li>
    </ul>
              <?php 
          if (isset($_GET['page'])) {
              $page=$_GET['page'];
              if ($page=='index_admin') { ?>
              
              <script type="text/javascript">
               window.location.href="index_admin.php"; 
              </script>

              <?php
              }
           } 
           ?>
          </li>

          <li class="">
            <a href="?page=pinjam_kembali">
              <i class="fa fa-exchange"></i> <span>Pinjam - Kembali</span>
            </span>
          </a>
          <?php 
          if (isset($_GET['page'])) {
              $page=$_GET['page'];
              if ($page=='pinjam_kembali') { ?>
              
              <script type="text/javascript">
               window.location.href="pinjam_kembali.php"; 
              </script>

              <?php
              }
           } 
           ?>
          
    </li>
    <li class="treeview">
            <a href="detail_pinjam.php">
              <i class="fa fa-exchange"></i> <span>Detail pinjam</span>
            </span>
          </a>
          
    </li>
  </li>
  <li class="">
    <a href="?page=ruang">
      <i class="fa fa-th"></i> <span>Ruang/Assets</span>
      <span class="pull-right-container">
      </span>
    </a>
    <?php 
          if (isset($_GET['page'])) {
              $page=$_GET['page'];
              if ($page=='ruang') { ?>
              
              <script type="text/javascript">
               window.location.href="ruang.php"; 
              </script>

              <?php
              }
           } 
           ?>
  </li>
  <li class="">
    <a href="?page=jenis">
      <i class="fa fa-th"></i> <span>Jenis</span>
      <span class="pull-right-container">
      </span>
    </a>
    <?php 
          if (isset($_GET['page'])) {
              $page=$_GET['page'];
              if ($page=='jenis') { ?>
              
              <script type="text/javascript">
               window.location.href="jenis.php"; 
              </script>

              <?php
              }
           } 
           ?>
  </li>
  <li class="treeview">
    <a href="laporan.php">
      <i class="fa fa-spinner"></i>
      <span>Laporan</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="data_barang.php"><i class="fa fa-circle-o-notch"></i> Data Barang</a></li>
      <li><a href="data_peminjam.php"><i class="fa fa-circle-o-notch"></i> Data peminjam</a></li>
      <li><a href="data_pengembalian.php"><i class="fa fa-circle-o-notch"></i> Data pengembalian</a></li>
    </ul>
  </li>
</li>
<li class="treeview">
    <a href="ruang.php">
      <i class="fa fa-th"></i> <span>Backup Database</span>
      <span class="pull-right-container">
      </span>
    </a>
  </li>
<!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->


<div id="wrapper">
       <!-- Navigation -->

       <div class="container" >
        <div class="graphs">
           <div class="xs">
             <div class="tab-content">
               <div class="tab-pane active" id="horizontal-form"  style="width:850px; ">
                    <div class="panel panel-default">
            <div class="panel-heading">Form Edit Inventaris</div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post">
                	<div class="form-group has-success"  style="width:490px; ">
                  <label class="control-label" for="inputSuccess">Nama</label>
                  <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $data['nama'];?>" placeholder="isi ...">
                </div>


                </form>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>