<!DOCTYPE html>
<html>
<head>
	<title>Help</title>
</head>
<body>
<h2>Cara penggunaan aplikasi :</h2>
<table>
	<tr>
	<td>1.</td>
	<td>Jika ingin login sebagai admin, maka username : admin1 dan password : admin1</td>
	</tr>
	<tr>
		<td>2.</td>
		<td>Jika ingin login sebagai operator, maka username : op1 dan password : operator</td>
	</tr>
	<tr>
	<td>3.</td>
		<td>Jika ingin login sebagai user, maka user memasukan NIP. login dengan NIP : 789179 sebagai manda</td>
	</tr>
	<tr>
		<td>4.</td>
		<td>Jika ingin melihat data inventaris sarana dan prasarana, maka lihat di fitur Data Barang. dimana sudah dikelompokan perjurusan masing-masing, tetapi jika ingin melihat data Asset tetap, maka lihat di fitur Asset tetap.</td>
	</tr>
	<tr>
		<td>5.</td>
		<td>Jika ingin meminjam barang sebagai Admin atau Operator, maka lihat di fitur pinjam barang. dan silahkan pilih barang sesuai yg ingin dipinjam. Anda hanya perlu memasukan nama peminjam dan jumlah yg akan pinjam. Dan jika ingin meminjam barang sebagai user, maka hanya perlu memasukan jumlah barang yg akan dipinjam.</td>
	</tr>
	<tr>
		<td>6.</td>
		<td>Jika ingin mengembalikan barang, maka lihat di fitur pengembalian. pengembalian barang dipisah antara Admin, operator dan user.</td>
	</tr>
	<tr>
		<td>7.</td>
		<td>Jika ingin melihat detail pinjam, tetap berada di fitur pengembalian.</td>
	</tr>
	<tr>
		<td>8.</td>
		<td>Jika ingin melihat data pengguna, maka lihat di fitur pengguna.</td>
	</tr>
	<tr>
		<td>9.</td>
		<td>Jika ingin melihat data Ruang dan Jenis maka lihat di fitur Master data.</td>
	</tr>
	<tr>
		<td>10.</td>
		<td>Jika ingin membackup database, lihat di fitur Backup. terbagi menjadi dua, yaitu backup database dan backup pertabel.</td>
	</tr>
	<tr>
		<td>11.</td>
		<td>Jika ingin export excel atau pdf, terdapat pada fitur masing masing.</td>
	</tr>
</table>
</body>
</html>